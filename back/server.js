const express = require('express');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const app = express();
const port = (process.env.NODE_ENV == 'test') ? 3001 : 3000;

const adapter = new FileSync('db.json');
const db = low(adapter);

db.defaults({
        number_list: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
    })
    .write();

app.use(express.static('public'));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/list', (req, res) => {
    let body = {};
    const number_list = db.get('number_list')
        .value();
    body['status'] = 200;
    body['data'] = {
        list: number_list
    };
    res.status(200).send(body);
});

app.get('/add/:number', (req, res) => {
    let body = {};
    db.get('number_list')
        .push(parseFloat(req.params.number))
        .write();
    body['status'] = 200;
    body['detail'] = "Added " + req.params.number + " to list.";
    res.status(200).send(body);
});

app.get('/del/index/:index', (req, res) => {
    let body = {};
    let number_list = db.get('number_list').value();
    number_list.splice(parseInt(req.params.index), 1);
    db.set('number_list', number_list)
        .write();
    body['status'] = 200;
    body['detail'] = "Removed number in position " + req.params.index + " from list.";
    res.status(200).send(body);
});

app.get('/del/value/:value', (req, res) => {
    let body = {};
    const number_list = db.get('number_list').value();
    db.set('number_list', number_list.filter(x => (x != req.params.value)))
        .write();
    body['status'] = 200;
    res.status(200).send("Removed number " + req.params.value + " from list.");
});

app.get('/average', (req, res) => {
    let body = {};
    let average = db.get('number_list')
        .value().reduce((x, n) => (x + n));
    average /= db.get('number_list').value().length;
    body['status'] = 200;
    body['data'] = {
        average: average
    };
    res.status(200).send(body);
});

app.get('/median', (req, res) => {
    let body = {};
    let number_list = db.get('number_list')
        .value();
    let median = 0;
    number_list.sort((a, b) => (a - b));
    if ((number_list.length % 2) == 0)
        median = (number_list[(number_list.length / 2) - 1] + number_list[number_list.length / 2]) / 2;
    else
        median = number_list[Math.ceil(number_list.length / 2)];
    body['status'] = 200;
    body['data'] = {
        median: median
    };
    res.status(200).send(body);
});

app.get('/map/add/:value', (req, res) => {
    let body = {};
    const number_list = db.get('number_list').value();
    db.set('number_list', number_list.map(x => (x + parseFloat(req.params.value))))
        .write();
    body['status'] = 200;
    body['detail'] = "Added " + req.params.value + " to all numbers of the list.";
    res.status(200).send(body);
});

app.get('/map/sub/:value', (req, res) => {
    let body = {};
    const number_list = db.get('number_list').value();
    db.set('number_list', number_list.map(x => (x - parseFloat(req.params.value))))
        .write();
    body['status'] = 200;
    body['detail'] = "Subtracted " + req.params.value + " to all numbers of the list.";
    res.status(200).send(body);
});

app.get('/map/mul/:value', (req, res) => {
    let body = {};
    const number_list = db.get('number_list').value();
    db.set('number_list', number_list.map(x => (x * parseFloat(req.params.value))))
        .write();
    body['status'] = 200;
    body['detail'] = "Multiplied all numbers of the list by " + req.params.value + ".";
    res.status(200).send(body);
});

app.get('/map/div/:value', (req, res) => {
    let body = {};
    if (parseFloat(req.params.value) == 0) {
        body['status'] = 500;
        body['error'] = "Divisor is 0.";
        res.status(500).send(body);
        return;
    }
    const number_list = db.get('number_list').value();
    db.set('number_list', number_list.map(x => (x / parseFloat(req.params.value))))
        .write();
    body['status'] = 200;
    body['detail'] = "Divided all numbers of the list by " + req.params.value + ".";
    res.status(200).send(body);
});

app.listen(port);

module.exports = {
    server: app,
    db: db
};