const host = window.location.hostname;

function addValue() {
    let value = document.getElementById("add").value;
    fetch("http://" + host + ":3000/add/" + value).then(function () {
        updateDisplay();
    });
}

function deleteValue() {
    let value = document.getElementById("del").value;
    fetch("http://" + host + ":3000/del/value/" + value).then(function () {
        updateDisplay();
    });
}

function mapValue(op) {
    let value = document.getElementById("map").value;
    fetch("http://" + host + ":3000/map/" + op + "/" + value).then(function () {
        updateDisplay();
    });
}

function updateDisplay() {
    fetch("http://" + host + ":3000/list").then(function (response) {
        response.json().then(function (json) {
            document.getElementById("number_list").innerHTML = json.data.list;
        });
    });
    fetch("http://" + host + ":3000/average").then(function (response) {
        response.json().then(function (json) {
            document.getElementById("average").innerHTML = json.data.average;
        });
    });
    fetch("http://" + host + ":3000/median").then(function (response) {
        response.json().then(function (json) {
            document.getElementById("median").innerHTML = json.data.median;
        });
    });
};

window.onload = function() {
  updateDisplay();
};