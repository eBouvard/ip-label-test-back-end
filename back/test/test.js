process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../server');
let server = app.server;
let db = app.db;
let should = chai.should();

chai.use(chaiHttp);

beforeEach(() => {
    db.set('number_list', [1, 5, 9, 6, 7])
        .write();
});

describe('Get list', () => {
    it('should get the number list', (done) => {
        chai.request(server)
            .get('/list')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.data.list.should.be.eql(db.get('number_list').value());
                done();
            });
    });
});

describe('Add number', () => {
    it('should add correct number to list', (done) => {
        const number = Math.random() * 999;
        chai.request(server)
            .get('/add/' + number)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value().slice(-1)[0].should.be.equal(number);
                done();
            });
    });
});

describe('Delete number by index', () => {
    it('should delete last number from list', (done) => {
        const number = db.get('number_list').value()[0];
        chai.request(server)
            .get('/del/index/' + 0)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value()[0].should.be.not.equal(number);
                done();
            });
    });
    it('should delete first number from list', (done) => {
        const number = db.get('number_list').value()[0];
        chai.request(server)
            .get('/del/index/' + JSON.stringify(db.get('number_list').value()))
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value()[0].should.be.not.equal(number);
                done();
            });
    });
});

describe('Delete number by value', () => {
    it('should delete correct number from list', (done) => {
        const number = db.get('number_list').value()[0];
        chai.request(server)
            .get('/del/value/' + number)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value().should.not.contain(number);
                done();
            });
    });
});

describe('Get average', () => {
    it('should get the average of the list', (done) => {
        chai.request(server)
            .get('/average')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.data.average.should.be.equal(
                    db.get('number_list').value().reduce((x, n) => (x + n)) / db.get('number_list').value().length
                );
                done();
            });
    });
});

describe('Get median', () => {
    it('should get the median of the list', (done) => {
        chai.request(server)
            .get('/median')
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                let number_list = db.get('number_list').value();
                let median = 0;
                number_list.sort((a, b) => (a - b));
                if ((number_list.length % 2) == 0)
                    median = (number_list[(number_list.length / 2) - 1] + number_list[number_list.length / 2]) / 2;
                else
                    median = number_list[Math.ceil(number_list.length / 2)];
                res.body.data.median.should.be.equal(median);
                done();
            });
    });
});

describe('Map list', () => {
    let number_list;
    beforeEach(() => {
        number_list = db.get('number_list').value();
    });
    it('should add 10 to all elements of the list', (done) => {
        chai.request(server)
            .get('/map/add/' + 10)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value().should.be.eql(
                    number_list.map(x => (x + 10))
                );
                done();
            });
    });
    it('should substract 2.5 to all elements of the list', (done) => {
        chai.request(server)
            .get('/map/sub/' + 2.5)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value().should.be.eql(
                    number_list.map(x => (x - 2.5))
                );
                done();
            });
    });
    it('should multiply all elements of the list by 6', (done) => {
        chai.request(server)
            .get('/map/mul/' + 6)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value().should.be.eql(
                    number_list.map(x => (x * 6))
                );
                done();
            });
    });
    it('should divide all elements of the list by 2', (done) => {
        chai.request(server)
            .get('/map/div/' + 2)
            .end((err, res) => {
                res.should.have.status(200);
                db.read();
                db.get('number_list').value().should.be.eql(
                    number_list.map(x => (x / 2))
                );
                done();
            });
    });
});