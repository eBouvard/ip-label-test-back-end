# Exercice évaluation recrutement R&D ip-label (back-end)
Ce dépot contient ma solution a l'exercice de recrutement R&D ip-label (back-end)
## Routes de l'API
|   Route               |   Effet                               |
|   -------------       |   :-------------:                     |
|   /                   |   Affiche une page Web simple         |
|   /list               |   Renvoie la liste de nombres         |
|   /add/*:number*        |   Ajoute `number` à la liste          |
|   /del/index/*:index*   |   Supprime l'élément d'index `index`  |
|   /del/value/*:value*   |   Supprime tous les élément de valeur `value`  |
|   /average            |   Calcule la moyenne                  |
|   /median             |   Calcule la médiane                  |
|   /map/add/*:value*     |   Ajoute `value` a tous les éléments de la liste.                     |
|   /map/sub/*:value*     |   Soustrait `value` a tous les éléments de la liste.                  |
|   /map/mul/*:value*     |   Multiplie tous les éléments de la liste par `value`.                |
|   /map/mul/*:value*     |   Divise tous les éléments de la liste par `value`.                   |

## Lancement local
### En développement avec Yarn
Utiliser la commande `yarn start` dans le dossier `back`
### En production avec Docker et Docker-Compose
Utiliser la commande `docker-compose up -d` dans le dossier racine
## Version en ligne
Une version en ligne est disponible au http://51.75.206.11:3000/
